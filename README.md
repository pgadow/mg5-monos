## Docker container for MadGraphAMC@NLO event generator

MadGraph is a integrated event generator for the high energy phisics and widely used in that field.

This docker image significantly simplifies the process to use MadGraph.
It contains the 2 mediator simplified dark matter model to study dark Higgs signatures.

### Usage

Run docker container and mount a local directory `output` inside the container for storing the event generation output:
```
mkdir output
docker run -it --rm --volume $PWD/output:/var/MG_outputs gitlab-registry.cern.ch/pgadow/mg5-monos
```

Within docker container:
```
/home/hep/MG5_aMC_v2_7_3/bin/mg5_aMC /home/hep/commands/monosbb/generate_sbb_LO_gq0p25_gx1p0_mzp1000_mhs30_mdm100.cmnd
```

Now after you exit the docker container, the output of the event generation will be avaliable in `output`.


### Look at the output

In the output directory there should be the output of the event generation
```
cd proc_sbb_lo_gq0p25_gx1p0_mzp1000_mhs30_mdm100

# inspect cross-sections and feynman graphs via browser
open index.html 

# retrieve output events
ls Events/run_01/

# look at parton level events (no hadronisation)
ls Events/run_01/unweighted_events.lhe.gz
root Events/run_01/unweighted_events.root

# look at HepMC events after hadronisation
# suggest using a tool for this: MadAnalysis, RIVET or pyhepmc
ls Events/run_01/tag_1_pythia8_events.hepmc.gz


# use MadAnalysis5 to analyse output on parton level
## inside docker container call madanalysis
/home/hep/MG5_aMC_v2_7_3/HEPTools/madanalysis5/madanalysis5/bin/ma5 --partonlevel
ma5> import output/proc_sbb_lo_gq0p25_gx1p0_mzp1000_mhs30_mdm100/Events/run_01/unweighted_events.lhe.gz
ma5> plot PT(invisible)
ma5> submit
## outside of docker container inspect results
open ANALYSIS_0/Output/HTML/MadAnalysis5job_0/index.html


# use MadAnalysis5 to analyse output on hadron level
## inside docker container call madanalysis
/home/hep/MG5_aMC_v2_7_3/HEPTools/madanalysis5/madanalysis5/bin/ma5 --hadronlevel
ma5> import proc_sbb_lo_gq0p25_gx1p0_mzp1000_mhs30_mdm100/Events/run_01/tag_1_pythia8_events.hepmc.gz
ma5> plot PT(invisible)
ma5> submit
## outside of docker container inspect results
open ANALYSIS_0/Output/HTML/MadAnalysis5job_0/index.html
```

### References:
- phenomenology paper: [Hunting the dark Higgs](https://arxiv.org/abs/1701.08780)
- 2 mediator dark matter theory framework: [How to save the WIMP](https://arxiv.org/abs/1606.07609)
- MadAnalysis tutorials: https://madanalysis.irmp.ucl.ac.be/wiki/tutorials 